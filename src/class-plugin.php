<?php

namespace WPezPluginsMarkerIo;

/**
 * Main and only plugin class, kinda God-y but for this, NBD.
 */
class Plugin {

	/**
	 * Prefix the options with this.
	 *
	 * @var string
	 */
	private $prefix;

	/**
	 * The submenu should be under what parent.
	 *
	 * @var string
	 */
	private $parent_slug;


	/**
	 * Let's __construct.
	 *
	 * @param string $str Pass in a different prefix.
	 */
	public function __construct( string $str = 'wpezplugins_markerio_' ) {

		$this->prefix = trim( $str );
		// TODO - add setter (or filter).
		$this->parent_slug = 'tools.php';
	}


	/**
	 * The callable function for the register_activation_hook.
	 *
	 * @return void
	 */
	public function activate() {

		$user_can_cap = trim( get_option( $this->prefix . 'user_can_cap' ), '' );

		if ( empty( esc_attr( $user_can_cap ) ) ) {

			// TODO - add filter to change default value.
			update_option( $this->prefix . 'user_can_cap', 'manage_options', true );
		}
	}

	/**
	 *  The callable function for the register_deactivation_hook
	 *
	 * @return void
	 */
	public function deactivate() {

		$deactivate_cleanup = trim( get_option( $this->prefix . 'deactivate_cleanup' ), '' );

		if ( ! empty( $deactivate_cleanup ) ) {

			delete_option( $this->prefix . 'widget_id' );
			delete_option( $this->prefix . 'user_can_cap' );
			delete_option( $this->prefix . 'deactivate_cleanup' );
		}	
	}

	/**
	 * Add the submenu page.
	 *
	 * @return void
	 */
	public function add_submenu_page() {

		// TODO - add filter to allow this to be customized.
		$funct = array( $this, 'settings' );

		add_submenu_page(
			$this->parent_slug,
			__( 'Marker.io Settings', 'wpezplugins-marker-io' ),
			__( 'Marker.io', 'wpezplugins-marker-io' ),
			'manage_options',
			'wpezplugins-marker-io',
			$funct,
		);
	}

	/**
	 * The settings for the submenu page.
	 *
	 * @return void
	 */
	public function settings() {
		?>
		<div class="wrap">
		<h1><?php echo esc_attr( __( 'Marker.io Settings', 'wpezplugins-marker-io' ) ); ?></h1>
			<form method="post" action="options.php">
			<?php
				settings_fields( $this->prefix . 'widget_section' );

				do_settings_sections( $this->prefix . 'options' );

				submit_button( __( 'Save Settings', 'wpezplugins-marker-io' ) );
			?>
			</form>
		</div>
		<?php
	}

	/**
	 * TODO - Undocumented function
	 *
	 * @return void
	 */
	public function display_settings() {

		// https://developer.wordpress.org/reference/functions/add_settings_section/
		add_settings_section(
			$this->prefix . 'widget_section',
			__( 'Widget', 'wpezplugins-marker-io' ),
			array( $this, 'display_header_options_content' ),
			$this->prefix . 'options',
		);

		// https://developer.wordpress.org/reference/functions/add_settings_field/
		add_settings_field(
			$this->prefix . 'widget_id',
			__( 'Destination ID', 'wpezplugins-marker-io' ),
			array( $this, 'widget_destination_id' ),
			$this->prefix . 'options',
			$this->prefix . 'widget_section',
		);

		add_settings_field(
			$this->prefix . 'user_can_cap',
			__( 'User Can Capability', 'wpezplugins-marker-io' ),
			array( $this, 'user_can_cap' ),
			$this->prefix . 'options',
			$this->prefix . 'widget_section',
		);

		add_settings_field(
			$this->prefix . 'deactivate_cleanup',
			__( 'Clean up on deactivate?', 'wpezplugins-marker-io' ),
			array( $this, 'deactivate_cleanup' ),
			$this->prefix . 'options',
			$this->prefix . 'widget_section',
		);

		// https://developer.wordpress.org/reference/functions/register_setting/
		register_setting(
			$this->prefix . 'widget_section',
			$this->prefix . 'widget_id',
			array( 'sanitize_callback' => array( $this, 'sanitize_text_field' ) ),
		);
		register_setting(
			$this->prefix . 'widget_section', 
			$this->prefix . 'user_can_cap',
			array( 'sanitize_callback' => array( $this, 'sanitize_text_field' ) ),
		);

		register_setting(
			$this->prefix . 'widget_section', 
			$this->prefix . 'deactivate_cleanup',
			array( 'sanitize_callback' => array( $this, 'sanitize_check' ) ),
		);
	}

	/**
	 * sanitize_callback for text fields.
	 *
	 * @param string $val
	 * @return string
	 */
	public function sanitize_text_field( $val ) {

		return sanitize_text_field( $val );
	}

	/**
	 * sanitize_callback for the checkbox.
	 *
	 * @param [type] $val
	 * @return void
	 */
	public function sanitize_check( $val ) {

		if ( '1' === $val ) {
			return $val;
		}
		return '';
	}

	/**
	 * Display the settings API's settings section header.
	 *
	 * @return void
	 */
	public function display_header_options_content() {
		echo 'Marker.io Admin > Destinations > {this site\'s destination} > Widget > Installation > Javascript snippet code > Look for: destination: { this value is the Destination ID }';
	}

	/**
	 * Render the input for: widget_destination_id.
	 *
	 * @return void
	 */
	public function widget_destination_id() {
		?>
			<input type="text" name="wpezplugins_markerio_widget_id" id="widget_id" value="<?php echo esc_attr( get_option( 'wpezplugins_markerio_widget_id' ) ); ?>" />
		<?php
	}

	/**
	 * Render the input for: user_can_cap.
	 *
	 * Ref: https://wordpress.org/support/article/roles-and-capabilities/
	 *
	 * @return void
	 */
	public function user_can_cap() {
		?>
		<input type="text" name="wpezplugins_markerio_user_can_cap" id="user_can_cap" value="<?php echo esc_attr( get_option( 'wpezplugins_markerio_user_can_cap' ) ); ?>" />
		<?php
		echo '<p>' . esc_attr( __( 'Leave blank to allow visitors who are not logged in to WP to use Marker.io.', 'wpezplugins-marker-io' ) );
		echo '<br>' . esc_attr( __( 'Blank is not recommended for production or any site that is directly accessible to the public.', 'wpezplugins-marker-io' ) ) . '</p>';
		echo '<p>' . esc_attr( __( '** Recommended value: ', 'wpezplugins-marker-io' ) ) . 'manage_options</p>';
		echo '<p>' . esc_attr( __( 'More Info: ', 'wpezplugins-marker-io' ) ) . '<a href="https://wordpress.org/support/article/roles-and-capabilities/" target="_blank">' . esc_attr( __( 'Roles and Capabilities on Wordpress.org', 'wpezplugins-marker-io' ) ) . '</a></p>';
	}

	/**
	 *  Render the input for: deactivate_cleanup.
	 *
	 * @return void
	 */
	public function deactivate_cleanup() {
		?>
		<!-- Here we are comparing stored value with 1. Stored value is 1 if user checks the checkbox otherwise empty string. -->
		<input type="checkbox" name="wpezplugins_markerio_deactivate_cleanup" value="1" <?php checked( 1, get_option( 'wpezplugins_markerio_deactivate_cleanup' ), true ); ?> />
		<?php
		echo '<span>' . esc_attr( __( 'Check to delete the plugin settings when deactivating the plugin.', 'wpezplugins-marker-io' ) ) . '</span>';
	}

	/**
	 * Render the Marker.io javascript.
	 *
	 * @return void
	 */
	public function wp_footer() {

		$user_can_cap = trim( get_option( 'wpezplugins_markerio_user_can_cap' ) );
		$widget_id    = trim( get_option( 'wpezplugins_markerio_widget_id' ) );

		if ( ! empty( esc_attr( $widget_id ) ) && ( empty( $user_can_cap ) || current_user_can( $user_can_cap ) ) ) {
			?>
<script>
window.markerConfig = {
	destination: '<?php echo esc_attr( $widget_id ); ?>',
};
</script>

<script>
!function(e,r,t){if(e.__Marker)return;e.__Marker={};var n=r.createElement("script");n.async=1,n.src="https://edge.marker.io/latest/shim.js";var s=r.getElementsByTagName("script")[0];s.parentNode.insertBefore(n,s)}(window,document);
</script>
			<?php
		}
	}

}
