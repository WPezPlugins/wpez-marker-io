# WPezMarker.io

### A WordPress plugin that adds the Marker.io javascript widget for users that have the specified capability, or all visitors. Once activated see: WP Admin > Tools > Marker.io


## "What is Marker.io?" 

[https://marker.io](https://marker.io?utm_source=gitlab&utm_content=wpez-marker-io)

Says their website: "Bug reporting made easy for everyone. Collect website feedback from your team and clients, without driving developers crazy."


## "Why should I care?"

Because Marker.io is an cost effectvie, easy to use, end-user-centric tool that captures feedback and automagically adds it to your favorite project support tools. Supported destinations include: 

__Issue tracking tools__

Bitbucket - [https://marker.io/bitbucket](https://marker.io/bitbucket?utm_source=gitlab&utm_content=wpez-marker-io)

Jira - [https://marker.io/jira](https://marker.io/jira?utm_source=gitlab&utm_content=wpez-marker-io)

GitHub - [https://marker.io/github](https://marker.io/github?utm_source=gitlab&utm_content=wpez-marker-io)

GitLab - [https://marker.io/gitlab](https://marker.io/gitlab?utm_source=gitlab&utm_content=wpez-marker-io)


__Project Management Tool__

Asana - [https://marker.io/asana](https://marker.io/asana?utm_source=gitlab&utm_content=wpez-marker-io)

Clickup -[https://marker.io/clickup](https://marker.io/clickup?utm_source=gitlab&utm_content=wpez-marker-io)

Clubhouse - [https://marker.io/clubhouse](https://marker.io/clubhouse?utm_source=gitlab&utm_content=wpez-marker-io)

Monday.com - [https://marker.io/monday-com](https://marker.io/monday-com?utm_source=gitlab&utm_content=wpez-marker-io)

Teamwork - [https://marker.io/teamwork](https://marker.io/teamwork?utm_source=gitlab&utm_content=wpez-marker-io)

Trello - [https://marker.io/trello](https://marker.io/trello?utm_source=gitlab&utm_content=wpez-marker-io)

Writke - [https://marker.io/trello](https://marker.io/wrike?utm_source=gitlab&utm_content=wpez-marker-io)


__Others__

BrowserStack - [https://marker.io/browserstack](https://marker.io/browserstack?utm_source=gitlab&utm_content=wpez-marker-io)

Email - [https://marker.io/email](https://marker.io/email?utm_source=gitlab&utm_content=wpez-marker-io)

Fullstory - [https://help.marker.io/en/articles/4729355](https://help.marker.io/en/articles/4729355-fullstory-integration?utm_source=gitlab&utm_content=wpez-marker-io)

Slack - [https://marker.io/slack](https://marker.io/slack?utm_source=gitlab&utm_content=wpez-marker-io)

Webhooks - [https://help.marker.io/en/articles/3738778](https://help.marker.io/en/articles/3738778-how-to-use-the-webhooks-integration?utm_source=gitlab&utm_content=wpez-marker-io)

Zapier - [https://help.marker.io/en/articles/3692677](https://help.marker.io/en/articles/3692677-how-to-connect-marker-io-to-zapier-using-the-webhooks-integration?utm_source=gitlab&utm_content=wpez-marker-io)


## TODO 

- Flesh out the README a bit more

