<?php
/**
 * Plugin Name: WPezPlugins: Marker.io
 * Plugin URI: https://gitlab.com/WPezPlugins/wpez-marker-io
 * Description: Adds the Marker.io javascript widget for users that have the specified capability, or all visitors. Once activated see: WP Admin Tools > Marker.io
 * Version: 0.0.5
 * Author: Mark "Chief Alchemist" Simchock for Alchemy United
 * Author URI: https://AlchemyUnited.com
 * License: GPLv2 or later
 * Text Domain: wpezplugins-marker-io
 *
 * @package WPezPluginsMarkerIo
 */

namespace WPezPluginsMarkerIo;

// No WP? Go away.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

$str_php_ver_comp = '7.2.0';
if ( version_compare( PHP_VERSION, $str_php_ver_comp, '<' ) ) {
	exit( sprintf( 'WPezPlugins: Marker.io requires PHP ' . esc_html( $str_php_ver_comp ) . ' or higher. Your WordPress site is using PHP %s.', PHP_VERSION ) );
}

/**
 * Loads the plugin class, inits it, and then the hooks.
 *
 * @param boolean $bool Disable by passing anything but true.
 * @return void
 */
function plugin( $bool = true ) {

	if ( true !== $bool ) {
		return;
	}

	require_once 'src/class-plugin.php';
	$new_plugin = new Plugin();

	register_activation_hook( __FILE__, array( $new_plugin, 'activate' ) );
	register_deactivation_hook( __FILE__, array( $new_plugin, 'deactivate' ) );

	add_action( 'admin_menu', array( $new_plugin, 'add_submenu_page' ) );
	add_action( 'admin_init', array( $new_plugin, 'display_settings' ) );
	add_action( 'wp_footer', array( $new_plugin, 'wp_footer' ) );
}
plugin();
